import asyncio
import os
import logging
import time
from typing import Union, Optional

import jwt
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi_versioning import VersionedFastAPI, version
from starlette.config import Config
from starlette.middleware.sessions import SessionMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse, RedirectResponse

from ska_src_authn_api.cache.redis import RedisCache
from ska_src_authn_api.common import constants
from ska_src_authn_api.common.exceptions import handle_exceptions, AuthorizationFlowNotSupported, \
                                                MismatchedStateError, PermissionDenied, TokenExchangeError, \
                                                TokenInvalidAudience, TokenInvalidScopes, TokenRefreshError, \
                                                TokenRequestError
from ska_src_authn_api.protocols.oidc import OIDCAuthorizationCodeFlow, OIDCBaseFlow, OIDCDeviceFlow
from ska_src_permissions_api.client.permissions import PermissionsClient


config = Config('.env')

# Instantiate FastAPI() allowing CORS. Middleware is added later to the instance of a VersionedApp.
#
app = FastAPI()
CORSMiddleware_params = {
    "allow_origins": ["*"],
    "allow_credentials": True,
    "allow_methods": ["*"],
    "allow_headers": ["*"]
}

# Set logging to use uvicorn logger.
#
logger = logging.getLogger("uvicorn")

# Get instance of IAM constants.
#
IAM_CONSTANTS = constants.IAM(client_conf_url=config.get('IAM_CLIENT_CONF_URL'))

# Instantiate caches if requested.
#
# Create two caches, one for token exchanges that are requested using only an access token (i.e. using the
# urn:ietf:params:oauth:grant-type:token-exchange grant type), and one using both an access token and refresh token
# (i.e. using the refresh_token grant type).
#
EXCHANGED_TOKEN_CACHE = None
if config.get('CACHE_TYPE', default='') == 'redis':
    EXCHANGED_TOKEN_CACHE = RedisCache(host=config.get('CACHE_HOST'), port=config.get('CACHE_PORT'), logger=logger)

# Instantiate the permissions client.
#
PERMISSIONS = PermissionsClient(config.get('PERMISSIONS_API_URL'))

# Store service start time.
#
SERVICE_START_TIME = time.time()

# Keep track of number of managed requests.
#
REQUESTS_COUNTER = 0
REQUESTS_COUNTER_LOCK = asyncio.Lock()

# Instances for OIDC flows.
#
# The OIDCBaseFlow instance is used for generic refresh/exchange grants.
#
OIDC_BASE_FLOW = OIDCBaseFlow(
    token_endpoint=IAM_CONSTANTS.iam_endpoint_token,
    client_id=config.get('API_IAM_CLIENT_ID'),
    client_secret=config.get('API_IAM_CLIENT_SECRET'))

# The OIDC device flow is used for headless auth.
#
OIDC_DEVICE_FLOW = OIDCDeviceFlow(
    device_authorization_endpoint=IAM_CONSTANTS.iam_endpoint_device_authorization,
    token_endpoint=IAM_CONSTANTS.iam_endpoint_token,
    client_id=config.get('API_IAM_CLIENT_ID'),
    client_secret=config.get('API_IAM_CLIENT_SECRET'))

# The OIDC authorization_code flow is used for browser based auth.
#
OIDC_AUTHORIZATION_CODE_FLOW = OIDCAuthorizationCodeFlow(
    authorization_endpoint=IAM_CONSTANTS.iam_endpoint_authorization,
    token_endpoint=IAM_CONSTANTS.iam_endpoint_token,
    client_id=config.get('API_IAM_CLIENT_ID'),
    client_secret=config.get('API_IAM_CLIENT_SECRET'))


# Dependencies.
# -------------
#
# Increment the request counter.
#
@handle_exceptions
async def increment_request_counter(request: Request) -> Union[dict, HTTPException]:
    global REQUESTS_COUNTER
    async with REQUESTS_COUNTER_LOCK:
        REQUESTS_COUNTER += 1


# Routes
# ------
#
@app.get("/login/{flow}", dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def login(request: Request, flow: str, redirect_uri: Optional[str] = None) -> JSONResponse:
    """
    Login using an OIDC flow. Current supported flows are:
      - authorization_code: code
      - device flow: device
      - legacy (modified version of authorization_code flow, similar to device): (legacy)
    """
    if flow == 'code':
        if not redirect_uri:
            redirect_uri = request.url_for("authorise_code")
        login_request, state = OIDC_AUTHORIZATION_CODE_FLOW.prepare_authorization_request(
            redirect_uri=redirect_uri, scopes=config.get('API_IAM_CLIENT_SCOPES', default=""),
            audience=config.get('API_IAM_CLIENT_AUDIENCE', default=""))
        request.session['state'] = state            # store state in session (browser only)
        return RedirectResponse(login_request.url)
    if flow == 'device':
        login_request = OIDC_DEVICE_FLOW.send_request(
            OIDC_DEVICE_FLOW.prepare_device_authorization_request(
                scopes=config.get('API_IAM_CLIENT_SCOPES', default="")))
        return JSONResponse(status_code=login_request.status_code, content=login_request.json())
    elif flow == 'legacy':
        if not redirect_uri:
            redirect_uri = request.url_for("authorise_code")
        login_request, state = OIDC_AUTHORIZATION_CODE_FLOW.prepare_authorization_request(
            redirect_uri=redirect_uri, scopes=config.get('API_IAM_CLIENT_SCOPES', default=""),
            audience=config.get('API_IAM_CLIENT_AUDIENCE', default=""))
        request.session['state'] = state            # store state in session (browser only)
        return JSONResponse({"authorization_uri": login_request.url})
    else:
        raise AuthorizationFlowNotSupported(flow)


@app.get("/code", dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def authorise_code(request: Request) -> JSONResponse:
    """
    Return the authorization response from an authorization_code flow from IAM.
    """
    code = request.query_params.get('code')
    state = request.query_params.get('state')
    if request.session.get('state'):                    # if browser
        if request.session.get('state') != state:       # check state matches last state in session (browser only)
            request.session['state'] = None
            raise MismatchedStateError
    request.session['state'] = None
    return JSONResponse({'code': code})


@app.get("/refresh", dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def refresh_token(request: Request, refresh_token: Union[str, None] = None) -> Union[JSONResponse, HTTPException]:
    """ Refresh an access token.

    This will always return access tokens with the original audience (authn-api).
    """
    refresh_token_before_exchange = refresh_token
    try:
        token_after_refresh = OIDC_BASE_FLOW.send_request(
            OIDC_BASE_FLOW.prepare_token_refresh_request(
                refresh_token=refresh_token_before_exchange,
                scopes=config.get('API_IAM_CLIENT_SCOPES'),
                audience=config.get('API_IAM_CLIENT_AUDIENCE')
            )
        ).json()
    except Exception as e:
        raise TokenRefreshError(repr(e))
    return token_after_refresh


@app.get("/token", dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def fetch_token(request: Request, code: Optional[str] = "", device_code: Optional[str] = "",
                      redirect_uri: Optional[str] = None) -> JSONResponse:
    """
    Retrieve a token from IAM.

    The flow is discerned by the combination of query parameters.

    :param code: The authorization code (authorization_code or legacy flow only).
    :param device_code: The device code (device flow only).
    :param redirect_uri: The redirect URI (authorization_code or legacy flow only, must match that from request).
    """
    if device_code and not code:
        response = OIDC_DEVICE_FLOW.send_request(
            OIDC_DEVICE_FLOW.prepare_device_access_token_request(
                device_code=device_code, audience=config.get('API_IAM_CLIENT_AUDIENCE')))
        token = response.json()
    elif code and not device_code:
        if not redirect_uri:
            redirect_uri = request.url_for("authorise_code")
        response = OIDC_AUTHORIZATION_CODE_FLOW.send_request(
            OIDC_AUTHORIZATION_CODE_FLOW.prepare_access_token_request(
                redirect_uri=redirect_uri, code=code))
        token = response.json()
    else:
        raise TokenRequestError("Combination of query parameters does not relate to recognised authorization flow.")

    access_token_decoded = jwt.decode(token['access_token'], options={"verify_signature": False})

    # check to see if token audience is as expected (user hasn't changed it!)
    if set(access_token_decoded['aud'].split()) != set(config.get("API_IAM_CLIENT_AUDIENCE").split()):
        raise TokenInvalidAudience(access_token_decoded['aud'], config.get("API_IAM_CLIENT_AUDIENCE"))

    # check to see if token scopes are as expected (user hasn't changed them!)
    scopes = token.get('scope', "")
    if set(scopes.split()) != set(config.get("API_IAM_CLIENT_SCOPES").split()):
        raise TokenInvalidScopes(scopes, config.get("API_IAM_CLIENT_SCOPES"))

    return JSONResponse({'token': token})


@app.get("/token/exchange/{service}", dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def exchange_token(request: Request, service: str, version: str = 'latest', try_use_cache: Optional[bool] = True,
                         access_token: Union[str, None] = None, refresh_token: Union[str, None] = None) \
        -> Union[JSONResponse, HTTPException]:
    """ Exchange a token for the version, {version}, of the service, {service}. """
    access_token_before_exchange = access_token

    # First, use the mandatory access token to establish if a token exchange to this service is authorised based on the
    # user group membership.
    rtn = PERMISSIONS.authorise_service_exchange(
        service=service, version=version, token=access_token_before_exchange).json()
    if not rtn.get('is_authorised', False):
        raise PermissionDenied

    # Then either use a cached token if it exists, or do a token exchange (either by refresh if refresh_token
    # provided or token_exchange grant if not).
    #
    # Note that using a refresh_token grant expires both the previous access and refresh token, so a cache isn't
    # possible.
    #
    token_after_exchange = None
    if try_use_cache and not refresh_token:
        logger.info("Checking if a cached token exists cache for service '{}'...".format(service))
        token_after_exchange = EXCHANGED_TOKEN_CACHE.get_token_for_service(service, access_token_before_exchange)
    if token_after_exchange:
        logger.info("Found valid cached token for service '{}'".format(service))
    else:
        logger.info("Couldn't find a valid cached token for service '{}', doing a token exchange...".format(service))
        if refresh_token:
            logger.info("- using refresh token")
            try:
                token_after_exchange = OIDC_BASE_FLOW.send_request(
                    OIDC_BASE_FLOW.prepare_token_exchange_request(
                        refresh_token=refresh_token,
                        audience=rtn.get('audience', config.get('API_IAM_CLIENT_AUDIENCE')),
                        scopes=config.get('API_IAM_CLIENT_SCOPES')
                    )
                ).json()
            except Exception as e:
                raise TokenRefreshError(repr(e))
        else:
            try:
                # Note that the "offline_access" scope is popped from the list of scopes during a token exchange,
                # other we get an "access denied" error. This is to be expected as it would allow a user to get
                # access to a refresh token when they may not have originally.
                token_after_exchange = OIDC_BASE_FLOW.send_request(
                    OIDC_BASE_FLOW.prepare_token_exchange_request(
                        access_token=access_token_before_exchange,
                        audience=rtn.get('audience', config.get('API_IAM_CLIENT_AUDIENCE')),
                        scopes=' '.join([scope for scope in config.get("API_IAM_CLIENT_SCOPES").split()
                                         if scope != "offline_access"])
                    )
                ).json()
            except Exception as e:
                raise TokenExchangeError(repr(e))
            if EXCHANGED_TOKEN_CACHE is not None:
                EXCHANGED_TOKEN_CACHE.set_token_for_service(service, access_token_before_exchange, token_after_exchange)
    return token_after_exchange


@app.get("/whoami", dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def whoami(request: Request, token: str) -> JSONResponse:
    """
    Decode a token.
    """
    return JSONResponse(jwt.decode(token, options={"verify_signature": False}))


@app.get('/ping')
@handle_exceptions
@version(1)
async def ping(request: Request):
    """ Service aliveness. """
    return JSONResponse({
        'status': "UP",
        'version': os.environ.get('SERVICE_VERSION'),
    })


@app.get('/health')
@handle_exceptions
@version(1)
async def health(request: Request):
    """ Service health. """

    # Dependent services.
    #
    # Permissions API
    #
    permissions_api_response = PERMISSIONS.ping()

    # Set return code dependent on criteria e.g. dependent service statuses
    #
    healthy_criteria = [
        permissions_api_response.status_code == 200,
    ]
    return JSONResponse(
        status_code=status.HTTP_200_OK if all(healthy_criteria) else status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            'uptime': round(time.time() - SERVICE_START_TIME),
            'number_of_managed_requests': REQUESTS_COUNTER,
            'dependent_services': {
                'permissions-api': {
                    'status': "UP" if permissions_api_response.status_code == 200 else "DOWN",
                }
            }
        }
    )

app = VersionedFastAPI(app, version_format='{major}', prefix_format='/v{major}')
app.add_middleware(CORSMiddleware, **CORSMiddleware_params)
app.add_middleware(SessionMiddleware, secret_key=config.get('SESSIONS_SECRET_KEY'))
