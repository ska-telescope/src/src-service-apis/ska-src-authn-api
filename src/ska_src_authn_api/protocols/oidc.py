import requests
import secrets

from ska_src_authn_api.common.exceptions import TokenRefreshError


class OIDCBaseFlow:
    def __init__(self, token_endpoint, client_id, client_secret):
        self.token_endpoint = token_endpoint
        self.client_id = client_id
        self.client_secret = client_secret

    def prepare_token_exchange_request(self, access_token=None, refresh_token=None, scopes='', audience=''):
        if refresh_token:
            request = self.prepare_token_refresh_request(refresh_token=refresh_token, scopes=scopes, audience=audience)
        elif access_token:
            request = requests.Request('POST', self.token_endpoint, data={
                'grant_type': 'urn:ietf:params:oauth:grant-type:token-exchange',
                'requested_token_type': 'urn:ietf:params:oauth:token-type:access_token',
                'subject_token': access_token,
                'subject_token_type': 'urn:ietf:params:oauth:token-type:access_token',
                'client_id': self.client_id,
                'client_secret': self.client_secret,
                'scope': scopes,
                'audience': audience
            }).prepare()
        else:
            raise TokenRefreshError("Neither an access token or refresh token has been provided.")
        return request

    def prepare_token_refresh_request(self, refresh_token, scopes='', audience=''):
        request = requests.Request('POST', self.token_endpoint, data={
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'scope': scopes,
            'audience': audience
        }).prepare()
        return request

    def send_request(self, request):
        response = requests.Session().send(request)
        response.raise_for_status()
        return response


class OIDCAuthorizationCodeFlow(OIDCBaseFlow):
    def __init__(self, authorization_endpoint, token_endpoint, client_id, client_secret):
        super().__init__(token_endpoint, client_id, client_secret)
        self.authorization_endpoint = authorization_endpoint

    def prepare_access_token_request(self, code, redirect_uri):
        request = requests.Request('POST', self.token_endpoint, data={
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': redirect_uri,
            'client_id': self.client_id,
            'client_secret': self.client_secret
        }).prepare()
        return request

    def prepare_authorization_request(self, redirect_uri, scopes='', audience=''):
        state = secrets.token_urlsafe(30)
        request = requests.Request('GET', self.authorization_endpoint, params={
            'response_type': 'code',
            'client_id': self.client_id,
            'redirect_uri': redirect_uri,
            'audience': audience,
            'scope': scopes,
            'state': state
        }).prepare()
        return request, state


class OIDCDeviceFlow(OIDCBaseFlow):
    def __init__(self, device_authorization_endpoint, token_endpoint, client_id, client_secret):
        super().__init__(token_endpoint, client_id, client_secret)
        self.device_authorization_endpoint = device_authorization_endpoint

    def prepare_device_access_token_request(self, device_code, audience):
        request = requests.Request('POST', self.token_endpoint, data={
            'grant_type': 'urn:ietf:params:oauth:grant-type:device_code',
            'device_code': device_code,
            'audience': audience,
            'client_id': self.client_id,
            'client_secret': self.client_secret
        }).prepare()
        return request

    def prepare_device_authorization_request(self, scopes=''):
        request = requests.Request('POST', self.device_authorization_endpoint, data={
            'client_id': self.client_id,
            'scope': scopes
        }).prepare()
        return request
