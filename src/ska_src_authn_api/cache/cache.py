from abc import ABC


class Cache(ABC):
    def delete_cache(self):
        raise NotImplementedError

    def dump_cache(self):
        raise NotImplementedError

    def get_token_for_service(self, api_client_access_token, service):
        raise NotImplementedError

    def set_token_for_service(self, api_client_access_token, exchanged_access_token, service):
        raise NotImplementedError

