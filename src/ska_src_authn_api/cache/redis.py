import json
import time

import jwt
import redis

from ska_src_authn_api.cache.cache import Cache


class RedisCache(Cache):
    def __init__(self, host, port, logger=None):
        """ A cache for access tokens.

        An entry in the cache takes the format of:

        before_exchange_token: {
            svc_1: after_exchange_token_1,
            svc_2: after_exchange_token_2
        }
        """
        self.logger = logger
        self.redis = redis.Redis(host=host, port=port, decode_responses=True)

    def delete_cache(self):
        for key in self.redis.scan_iter("*"):
            self.redis.delete(key)

    def dump_cache(self):
        output = {}
        for key in self.redis.scan_iter("*"):
            output[key] = json.loads(self.redis.get(key))
        return output

    def get_token_for_service(self, service, before_exchange_token):
        entry = self.redis.get(before_exchange_token)
        if entry:
            service_exchanged_token = json.loads(entry).get(service)
            if service_exchanged_token:
                access_token_decoded = jwt.decode(
                    service_exchanged_token['access_token'], options={"verify_signature": False})
                if int(time.time()) < access_token_decoded['exp'] - 60:
                    return service_exchanged_token
                else:
                    if self.logger:
                        self.logger.info("Cached token has expired.")
        return None

    def set_token_for_service(self, service, before_exchange_token, after_exchange_token):
        entry = self.redis.get(before_exchange_token)
        if entry:
            new_entry = json.loads(entry)
            new_entry[service] = after_exchange_token
        else:
            new_entry = {
                service: after_exchange_token
            }
        self.redis.set(before_exchange_token, json.dumps(new_entry))
