import requests

from ska_src_authn_api.common.exceptions import handle_client_exceptions


class AuthenticationClient:
    def __init__(self, api_url, session=None):
        self.api_url = api_url
        if session:
            self.session = session
        else:
            self.session = requests.Session()

    @handle_client_exceptions
    def exchange_token(self, service: str, version: str = 'latest', try_use_cache: bool = True,
                       access_token: str = None, refresh_token: str = None):
        """ Exchange a token for a service.

        :param str service: The service name.
        :param str version: The service version.
        :param bool try_use_cache: Try to get a token from the cache rather then IAM.
        :param str access_token: An access token.
        :param str refresh_token: A refresh token.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        exchange_token_endpoint = "{api_url}/token/exchange/{service}".format(api_url=self.api_url, service=service)
        params = {
            "version": version,
            "try_use_cache": try_use_cache,
            "access_token": access_token,
            "refresh_token": refresh_token
        }
        resp = self.session.get(exchange_token_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def health(self):
        """ Get the service health.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        health_endpoint = "{api_url}/health".format(api_url=self.api_url)
        resp = self.session.get(health_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def login(self, flow="code", redirect_uri=""):
        """ Start a login flow.

        :param str flow: The OIDC flow (code||device||legacy).
        :param str redirect_uri: The redirect URI (code and legacy only).

        :return: A requests response.
        :rtype: requests.models.Response
        """
        login_endpoint = "{api_url}/login/{flow}".format(api_url=self.api_url, flow=flow)
        params = {
            "redirect_uri": redirect_uri
        }

        resp = self.session.get(login_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def ping(self):
        """ Ping the service.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        ping_endpoint = "{api_url}/ping".format(api_url=self.api_url)
        resp = self.session.get(ping_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def refresh_token(self, refresh_token: str = None):
        """ Get an access token given a refresh token.

        :param str refresh_token: A refresh token.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        refresh_token_endpoint = "{api_url}/refresh".format(api_url=self.api_url)
        params = {
            "refresh_token": refresh_token
        }
        resp = self.session.get(refresh_token_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def token(self, code="", device_code="", redirect_uri=""):
        """ Complete an authorisation_code grant flow.

        :param code: The authorization code (authorization_code or legacy flow only).
        :param device_code: The device code (device flow only).
        :param redirect_uri: The redirect URI (authorization_code or legacy flow only, must match that from request).

        :return: A requests response.
        :rtype: requests.models.Response
        """
        login_endpoint = "{api_url}/token".format(api_url=self.api_url)
        params = {
            "code": code,
            "device_code": device_code,
            "redirect_uri": redirect_uri
        }
        resp = self.session.get(login_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def whoami(self, token: str = None):
        """ Introspect a token.

        :param str token: An access token.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        whoami_endpoint = "{api_url}/whoami".format(api_url=self.api_url)
        params = {
            "token": token
        }
        resp = self.session.get(whoami_endpoint, params=params)
        resp.raise_for_status()
        return resp
