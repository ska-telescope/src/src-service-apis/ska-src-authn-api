import requests
import traceback
from functools import wraps

from fastapi import HTTPException, status


def handle_client_exceptions(func):
    """ Decorator to handle client exceptions. """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            status_code = e.response.status_code
            detail = f"HTTP error occurred: {e}, response: {e.response.text}"
            raise HTTPException(status_code=status_code, detail=detail)
        except HTTPException as e:
            raise e
        except CustomException as e:
            raise Exception(message=e.message)
        except CustomHTTPException as e:
            raise HTTPException(status_code=e.http_error_status, detail=e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise HTTPException(status_code=500, detail=detail)
    return wrapper


def handle_exceptions(func):
    """ Decorator to handle server exceptions. """
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            status_code = e.response.status_code
            detail = f"HTTP error occurred: {e}, response: {e.response.text}"
            raise HTTPException(status_code=status_code, detail=detail)
        except HTTPException as e:
            raise e
        except CustomException as e:
            raise Exception(message=e.message)
        except CustomHTTPException as e:
            raise HTTPException(status_code=e.http_error_status, detail=e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise HTTPException(status_code=500, detail=detail)
    return wrapper


class CustomException(Exception):
    """ Class that all custom exceptions must inherit in order for exception to be caught by the
    handle_exceptions decorator.
    """
    pass


class IAMEndpointNotFoundInWellKnown(CustomException):
    def __init__(self, endpoint):
        self.message = "Error setting IAM {} endpoint, not found in .well_known".format(endpoint)
        super().__init__(self.message)


class CustomHTTPException(Exception):
    """ Class that all custom HTTP exceptions must inherit in order for exception to be caught by
    the handle_exceptions decorator.
    """
    pass


class AuthorizationFlowNotSupported(CustomHTTPException):
    def __init__(self, requested_authorization_flow):
        self.message = "The authorization flow {} is not supported".format(requested_authorization_flow)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class PermissionDenied(CustomHTTPException):
    def __init__(self):
        self.message = "You do not have permission to access this resource."
        self.http_error_status = status.HTTP_403_FORBIDDEN
        super().__init__(self.message)


class MismatchedStateError(CustomHTTPException):
    def __init__(self):
        self.message = "States do not match."
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class TokenExchangeError(CustomHTTPException):
    def __init__(self, reason):
        self.message = "Error during token exchange: {}".format(reason)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class TokenInvalidAudience(CustomHTTPException):
    def __init__(self, presented, expected):
        self.message = "Token has an incorrect audience, got '{}' expected '{}'".format(
            repr(presented), repr(expected))
        self.http_error_status = status.HTTP_401_UNAUTHORIZED
        super().__init__(self.message)


class TokenInvalidScopes(CustomHTTPException):
    def __init__(self, presented, expected):
        self.message = "Token has incorrect scopes, got '{}' expected '{}'".format(
            repr(presented), repr(expected))
        self.http_error_status = status.HTTP_401_UNAUTHORIZED
        super().__init__(self.message)


class TokenRefreshError(CustomHTTPException):
    def __init__(self, reason):
        self.message = "Error during token refresh: {}".format(reason)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class TokenRequestError(CustomHTTPException):
    def __init__(self, reason):
        self.message = "Error during token request: {}".format(reason)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)
