FROM python:3.8-bullseye

USER root

RUN groupadd user
RUN adduser --system --no-create-home --disabled-password --shell /bin/bash user

COPY --chown=user . /opt/ska-src-authn-api

RUN cd /opt/ska-src-authn-api && python3 -m pip install -e . --extra-index-url https://gitlab.com/api/v4/projects/48060714/packages/pypi/simple

WORKDIR /opt/ska-src-authn-api

ENV API_ROOT_PATH ''
ENV API_IAM_CLIENT_ID ''
ENV API_IAM_CLIENT_SECRET ''
ENV API_IAM_CLIENT_SCOPES ''
ENV API_IAM_CLIENT_AUDIENCE ''
ENV IAM_CLIENT_CONF_URL ''
ENV CACHE_TYPE=''
ENV CACHE_HOST=''
ENV CACHE_PORT=''
ENV PERMISSIONS_API_URL ''

ENTRYPOINT ["/bin/bash", "etc/docker/init.sh"]
