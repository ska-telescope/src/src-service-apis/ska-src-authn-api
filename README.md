# SKA SRC Auth API

[[_TOC_]]

## Overview

This API exposes endpoints related to SRCNet authentication and token exchanges.

## Structure

The repository is structured as follows:

```
.
├── .env.template
├── .gitlab-ci.yml
├── bin
├── docker-compose.yml
├── Dockerfile
├── etc
│   ├── docker
│   │   └── init.sh
│   └── helm
│       ├── Chart.yaml
│       ├── templates
│       └── values.yaml.template
├── LICENSE
├── README.md
├── requirements.txt
├── setup.py
├── src
│   └── ska_src_authn_api
│       ├── cache
│       ├── client
│       ├── common
│       └── rest
├── TODO.md
└── VERSION
```

## Authentication

User authentication follows a slightly modified version of the OIDC authorization_code flow and closely resembles a 
standard OIDC device_code flow. This is due to limitations in the underlying OIDC library being used (Authlib). This 
could be switched in the future should a suitable substitute for the OAuth2 framework be identified.

The flow contains two paths. The first is from the device that the user is attempting to authenticate from, and the 
second occurs in a browser. The flow proceeds like:

1. User calls /login endpoint and is redirected to IAM login
2. User logs in to IAM and is redirected to the service’s /code endpoint, yielding an authorisation code
3. User presents this authorisation code to the /token endpoint to get an access token token

The result of this authentication process is that the user is given an access token with an audience limited to use at 
the Authentication API only. This token is expected to be exchanged when access to other services is required.

## Token exchange

For access to another SRCNet service (hereafter referred to as the “requested service”), the authentication token needs 
to be exchanged for one with an audience that the requested service expects. For this purpose, the Authentication API 
contains a mechanism to request a token exchange from IAM.

Before a token exchange for access to the requested service can occur, it must first be established that the user has 
the correct permissions to do so. This permissions check is handled by the authentication service by making a separate 
request to the Permissions API. The token obtained from the authentication service and the name of the service that the 
user is requesting access to is presented to the Permissions API /authorise/exchange endpoint, and the permissions 
service returns the result of the permissions check, which is essentially a yes/no response and, if yes, the audience 
required by the requested service.

The permissions check is a simple check of group membership. For example, if a user wants access to the Data Management 
API, they must be a member of the /services/data-management-api subgroup (actual group names may vary depending on how 
the permissions service has been set up).

If the permissions check proceeds successfully, the Authentication API either requests an appropriately exchanged token 
from IAM (with the audience returned from the check), or selects an existing one from a cache. This exchanged token can 
then be used at the requested service’s API.

The flow proceeds like:

1.  User presents Authentication API access token to /token/exchange endpoint
2.  Authentication API access token is presented to the Permissions API /authorise/exchange endpoint, which checks that 
    the user’s token is valid to be exchanged for access to another service and, if so, returns the audience 
    representing the requested service 
3. If the user is permitted to do so, the token is exchanged at IAM (or a valid token is taken from the service’s cache)
   for an access token (*)  with the audience limited to use at the requested service 
4. The user then presents this exchanged token to the requested service for access. Further permissions checks will be 
   done depending on what action is wanting to be performed at this service.

## Development

Makefile targets have been included to facilitate easier and more consistent development against this API. The general 
recipe is as follows:

1. Depending on the fix type, create a new major/minor/patch branch, e.g. 
    ```bash
    $ make patch-branch NAME=some-name
    ```
    Note that this both creates and checkouts the branch.
2. Make your changes.
3. Add your changes to the branch:
    ```bash
   $ git add ...
    ```
4. Either commit the changes manually (if no version increment is needed) or bump the version and commit, entering a 
   commit message when prompted:
    ```bash
   $ make bump-and-commit
    ```
5. Push the changes upstream when ready:
    ```bash
   $ make push
    ```

Note that the CI pipeline will fail if python packages with the same semantic version are committed to the GitLab 
Package Registry.

## Deployment

Deployment is managed by docker-compose or helm.

The docker-compose file can be used to bring up the necessary services locally i.e. the REST API, setting the mandatory 
environment variables. Sensitive environment variables, including those relating to the IAM client, should be kept in 
`.env` files to avoid committing them to the repository.

There is also a helm chart for deployment onto a k8s cluster.

### Example via docker-compose

Edit the .env.template file accordingly and rename to .env, then:

```bash
eng@ubuntu:~/SKAO/ska-src-authn-api$ docker-compose up
```

### Example via Helm

After creating a `values.yaml` (template in `/etc/helm/`):

```bash
$ create namespace ska-src-authn-api
$ helm install --namespace ska-src-authn-api ska-src-authn-api .
```
